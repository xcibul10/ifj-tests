var
  r1: real;
  r2: real;
  i1: integer;
  i2: integer;
  s1: string;
  s2: string;

function pes(i: integer; r: real) : integer;
begin

end;

begin
  r1 := 1.7;
  r2 := 2.5;
  i1 := 3;
  i2 := 4;
  s1 := 'abc';
  s2 := 'def';

  write('r1: ', r1, ''#10'');
  write('r2: ', r2, ''#10'');
  write('i1: ', i1, ''#10'');
  write('i2: ', i2, ''#10'');
  write('s1: ''', s1, ''''#10'');
  write('s2: ''', s2, ''''#10'');

  write('r1 * r2 = ', r1 * r2, ''#10'');
  write('i1 * i2 = ', i1 * i2, ''#10'');
  write('i1 * r2 = ', i1 * r2, ''#10'');
  write('r1 * i2 = ', r1 * i2, ''#10'');

  write('r1 + r2 = ', r1 + r2, ''#10'');
  write('i1 + i2 = ', i1 + i2, ''#10'');
  write('i1 + r2 = ', i1 + r2, ''#10'');
  write('r1 + i2 = ', r1 + i2, ''#10'');
  write('s1 + s2 = ', s1 + s2, ''#10'');

  write('r1 - r2 = ', r1 - r2, ''#10'');
  write('i1 - i2 = ', i1 - i2, ''#10'');
  write('i1 - r2 = ', i1 - r2, ''#10'');
  write('r1 - i2 = ', r1 - i2, ''#10'');

  write('r1 / r2 = ', r1 / r2, ''#10'');
  write('i1 / i2 = ', i1 / i2, ''#10'');
  write('i1 / r2 = ', i1 / r2, ''#10'');
  write('r1 / i2 = ', r1 / i2, ''#10'');

  write('i1 < i2 = ', i1 < i2, ''#10'');
  write('i2 < i1 = ', i2 < i1, ''#10'');
  write('r1 < r2 = ', r1 < r2, ''#10'');
  write('r2 < r1 = ', r2 < r1, ''#10'');
  write('s1 < s2 = ', s1 < s2, ''#10'');
  write('s2 < s1 = ', s2 < s1, ''#10'');

  write('i1 > i1 = ', i1 > i1, ''#10'');
  write('i2 > i1 = ', i2 > i1, ''#10'');
  write('r1 > r2 = ', r1 > r2, ''#10'');
  write('r2 > r1 = ', r2 > r1, ''#10'');

  write('4 <= 4 = ', 4 <= 4, ''#10'');
  write('r1 <= 8.4 = ', r1 <= 8.4, ''#10'');

  write('4 >= 4 = ', 4 >= 4, ''#10'');
  write('r2 >= r1 = ', r2 >= r1, ''#10'');
  write('s1 >= s2 = ', s1 >= s2, ''#10'');
  
  write('r1 <> r2 = ', r1 <> r2, ''#10'');
  write('i1 <> i2 = ', i1 <> i2, ''#10'');
  write('s1 <> s2 = ', s1 <> s2, ''#10'')
end.